<?php
// =============================== Holo Portfolio Carousel Widget ======================================
class Holo_PortfolioCarouselWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_holo_portfolio_carousel', 'description' => esc_html__('Holo - Portfolio Carousel', "holo-portfolio") );
		parent::__construct('holo-portfolio-carousel-widget', esc_html__('Holo - Portfolio Carousel',"holo-portfolio"), $widget_ops);
	}
	
	function widget( $args, $instance ) {
		global $wpdb, $comments, $comment;

		extract($args, EXTR_SKIP);
		$title      = apply_filters('widget_holo_portfolio_carousel_title', empty($instance['title']) ? '' : $instance['title']);
        $nospace    = apply_filters('widget_holo_portfolio_carousel_nospace', empty($instance['nospace']) ? '' : $instance['nospace']);
        $cat        = apply_filters('widget_holo_portfolio_carousel_cat', empty($instance['cat']) ? '' : $instance['cat']);
        $showposts  = apply_filters('widget_holo_portfolio_carousel_showposts', empty($instance['showposts']) ? '' : $instance['showposts']);
		
        $scparams = '';
        if(trim($class)!=''){
            $scparams .= ' title="'.esc_attr($title).'"';
        }
        
        if(trim($nospace)!=''){
            $scparams .= ' nospace="'.esc_attr($nospace).'"';
        }
        
        if(trim($cat)!=''){
            $scparams .= ' cat="'.esc_attr($cat).'"';
        }
        
        if(trim($showposts)!=''){
            $scparams .= ' showposts="'.esc_attr($showposts).'"';
        }
        
        echo do_shortcode('[portfolio_carousel '.$scparams.']');
	}
	
	function update($new_instance, $old_instance) {				
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
		$instance['title'] = (isset($instance['title']))? $instance['title'] : "";
		$instance['cat'] = (isset($instance['cat']))? $instance['cat'] : "";
        $instance['nospace'] = (isset($instance['nospace']))? $instance['nospace'] : "";
        $instance['showposts'] = (isset($instance['showposts']))? $instance['showposts'] : "";
		
        $arrsval = array(
            'yes' => __('Yes', 'holo-portfolio'),
            'no' => __('No', 'holo-portfolio')
        );
        
        $class = esc_attr($instance['class']);
		$cat = esc_attr($instance['cat']);
        $nospace = esc_attr($instance['nospace']);
		$showposts = esc_attr($instance['showposts']);
        

        ?>
            <p><label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title:', "holo-portfolio"); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></label></p>
			
            <p><label for="<?php echo esc_attr( $this->get_field_id('cat') ); ?>"><?php esc_html_e('Portfolio Category Slug:', "holo-portfolio" ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('cat') ); ?>" name="<?php echo esc_attr( $this->get_field_name('cat') ); ?>" type="text" value="<?php echo esc_attr( $cat ); ?>" /></label></p>
            
            <p><label for="<?php echo esc_attr( $this->get_field_id('nospace') ); ?>"><?php esc_html_e('No Space:', "holo-portfolio" ); ?> 
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('nospace') ); ?>" name="<?php echo esc_attr( $this->get_field_name('nospace') ); ?>">
                    <?php foreach($arrsval as $arrval => $arrname ){ ?>
                        <?php $selected = ($arrval==$nospace)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $arrval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $arrname ); ?></option>
                    <?php }?>
                </select>
            </label></p>
            
            <p><label for="<?php echo esc_attr( $this->get_field_id('showposts') ); ?>"><?php esc_html_e('Showposts:', "holo-portfolio" ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('showposts') ); ?>" name="<?php echo esc_attr( $this->get_field_name('showposts') ); ?>" type="text" value="<?php echo esc_attr( $showposts ); ?>" /></label></p>
        <?php 
    }
}
?>