<?php
// =============================== Holo Portfolio Filter Widget ======================================
class Holo_PortfolioFilterWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_holo_portfolio_filter', 'description' => esc_html__('Holo - Portfolio Filter', "holo-portfolio") );
		parent::__construct('holo-portfolio-filter-widget', esc_html__('Holo - Portfolio Filter',"holo-portfolio"), $widget_ops);
	}
	
	function widget( $args, $instance ) {
		global $wpdb, $comments, $comment;

		extract($args, EXTR_SKIP);
		$title      = apply_filters('widget_holo_portfolio_filter_title', empty($instance['title']) ? '' : $instance['title']);
        $nospace    = apply_filters('widget_holo_portfolio_filter_nospace', empty($instance['nospace']) ? '' : $instance['nospace']);
        $cat        = apply_filters('widget_holo_portfolio_filter_cat', empty($instance['cat']) ? '' : $instance['cat']);
        $type        = apply_filters('widget_holo_portfolio_filter_type', empty($instance['type']) ? '' : $instance['type']);
        $col        = apply_filters('widget_holo_portfolio_filter_col', empty($instance['col']) ? '' : $instance['col']);
        $showposts  = apply_filters('widget_holo_portfolio_filter_showposts', empty($instance['showposts']) ? '' : $instance['showposts']);
		
        $scparams = '';
        if(trim($class)!=''){
            $scparams .= ' title="'.esc_attr($title).'"';
        }
        
        if(trim($nospace)!=''){
            $scparams .= ' nospace="'.esc_attr($nospace).'"';
        }
        
        if(trim($cat)!=''){
            $scparams .= ' cat="'.esc_attr($cat).'"';
        }
        
        if(trim($col)!=''){
            $scparams .= ' col="'.esc_attr($col).'"';
        }
        
        if(trim($showposts)!=''){
            $scparams .= ' showposts="'.esc_attr($showposts).'"';
        }
        
        echo do_shortcode('[portfolio_filter '.$scparams.']');
	}
	
	function update($new_instance, $old_instance) {				
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
		$instance['title'] = (isset($instance['title']))? $instance['title'] : "";
		$instance['cat'] = (isset($instance['cat']))? $instance['cat'] : "";
        $instance['type'] = (isset($instance['type']))? $instance['type'] : "";
        $instance['col'] = (isset($instance['col']))? $instance['col'] : "";
        $instance['nospace'] = (isset($instance['nospace']))? $instance['nospace'] : "";
        $instance['showposts'] = (isset($instance['showposts']))? $instance['showposts'] : "";
        
        $cols = array(
            '3' => __('3 Columns', 'holo-portfolio'),
            '4' => __('4 Columns', 'holo-portfolio'),
            '5' => __('5 Columns', 'holo-portfolio')
        );
        
        $grids = array(
            'grid' => __('grids', 'holo-portfolio'),
            'classic' => __('Classic', 'holo-portfolio'),
            'masonry' => __('Masonry', 'holo-portfolio')
        );
		
        $arrsval = array(
            'yes' => __('Yes', 'holo-portfolio'),
            'no' => __('No', 'holo-portfolio')
        );
        
        $title = esc_attr($instance['title']);
		$cat = esc_attr($instance['cat']);
        $col = esc_attr($instance['col']);
        $type = esc_attr($instance['type']);
        $nospace = esc_attr($instance['nospace']);
		$showposts = esc_attr($instance['showposts']);
        

        ?>
            <p><label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title:', "holo-portfolio"); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></label></p>
			
            <p><label for="<?php echo esc_attr( $this->get_field_id('cat') ); ?>"><?php esc_html_e('Portfolio Category Slug:', "holo-portfolio" ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('cat') ); ?>" name="<?php echo esc_attr( $this->get_field_name('cat') ); ?>" type="text" value="<?php echo esc_attr( $cat ); ?>" /></label></p>
            
            <p><label for="<?php echo esc_attr( $this->get_field_id('col') ); ?>"><?php esc_html_e('Number of Columns:', "holo-portfolio" ); ?> 
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('col') ); ?>" name="<?php echo esc_attr( $this->get_field_name('col') ); ?>">
                    <?php foreach($cols as $colval => $colname){ ?>
                        <?php $selected = ($colval==$col)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $colval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $colname ); ?></option>
                    <?php }?>
                </select>
            </label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('type') ); ?>"><?php esc_html_e('Type:', "holo-portfolio" ); ?> 
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('type') ); ?>" name="<?php echo esc_attr( $this->get_field_name('type') ); ?>">
                    <?php foreach($grids as $gridval => $gridname){ ?>
                        <?php $selected = ($gridval==$type)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $gridval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $gridname ); ?></option>
                    <?php }?>
                </select>
            </label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('nospace') ); ?>"><?php esc_html_e('No Space:', "holo-portfolio" ); ?> 
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('nospace') ); ?>" name="<?php echo esc_attr( $this->get_field_name('nospace') ); ?>">
                    <?php foreach($arrsval as $arrval => $arrname ){ ?>
                        <?php $selected = ($arrval==$nospace)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $arrval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $arrname ); ?></option>
                    <?php }?>
                </select>
            </label></p>
            
            <p><label for="<?php echo esc_attr( $this->get_field_id('showposts') ); ?>"><?php esc_html_e('Showposts:', "holo-portfolio" ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('showposts') ); ?>" name="<?php echo esc_attr( $this->get_field_name('showposts') ); ?>" type="text" value="<?php echo esc_attr( $showposts ); ?>" /></label></p>
        <?php 
    }
}
?>